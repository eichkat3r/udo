#!/bin/bash

if [[ $UID != 0 ]]; then
	echo "sie brauchen root privilegien um den installer auszuführen"
	echo ""
	echo "führen sie sudo ./install.sh aus und versuchen sie es nochmal"
	exit
fi


if ! type cowsay 2>/dev/null; then
	echo "bitte installiere cowsay um fortzufahren"
	exit
fi

echo "installiere udo..."

cp udo.cow /usr/share/cows/
cp udo.sh /usr/bin/udo

echo "installation abgeschlossen sie sollten den rechner jetzt neu starten"
echo ""
echo "keine ahnung ob das wirklich notwendig ist aber bei windows steht das"
echo "da auch immer wenn man was installiert hat"
