#!/bin/bash

if [[ -z "$@" ]]; then
	echo "bitte geben sie ein kommando ein das udo ausführen soll"
	exit
fi

cowsay -f udo "hallo ich bin udo ihr persönlicher assistent. ihr kommando wird in kürze ausgeführt..."
echo -e "\n\n\n"
echo "–––––––––––––––––––––––––––––––––––––––––––"
echo -e "\n\n\n"
sleep 10

sudo "$@"
