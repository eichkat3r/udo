# udo

udo ist wie sudo nur dass das übergebene kommando von einem persönlichen
assistenten namens udo ausgeführt wird

![](screenshot.png)

zwischen dem programmaufruf und der ausführung gibt es eine kurze verzögerung
weil udo erstmal ein wenig zeit braucht um sich zu sortieren denn er möchte
seine arbeit ordentlich machen
